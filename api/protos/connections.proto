syntax = "proto3";

package mizer;

service ConnectionsApi {
  rpc GetConnections (GetConnectionsRequest) returns (Connections) {}

  rpc MonitorDmx (MonitorDmxRequest) returns (MonitorDmxResponse) {}
  rpc MonitorMidi (MonitorMidiRequest) returns (stream MonitorMidiResponse) {}

  rpc AddArtnetConnection (ArtnetConfig) returns (Connections) {}
  rpc AddSacnConnection (SacnConfig) returns (Connections) {}

  rpc GetMidiDeviceProfiles (GetDeviceProfilesRequest) returns (MidiDeviceProfiles) {}

  rpc DeleteConnection (Connection) returns (Connections) {}
  rpc ConfigureConnection (ConfigureConnectionRequest) returns (Connection) {}
}

message MonitorDmxRequest {
  string outputId = 1;
}

message MonitorDmxResponse {
  repeated MonitorDmxUniverse universes = 1;
}

message MonitorDmxUniverse {
  uint32 universe = 1;
  bytes channels = 2;
}

message MonitorMidiRequest {
  string name = 1;
}
message MonitorMidiResponse {
  uint64 timestamp = 2;
  oneof message {
    NoteMsg cc = 3;
    NoteMsg noteOff = 4;
    NoteMsg noteOn = 5;
    SysEx sysEx = 6;
    bytes unknown = 7;
  }

  message NoteMsg {
    uint32 channel = 1;
    uint32 note = 2;
    uint32 value = 3;
  }

  message SysEx {
    uint32 manufacturer1 = 1;
    uint32 manufacturer2 = 2;
    uint32 manufacturer3 = 3;
    uint32 model = 4;
    bytes data = 5;
  }
}

message GetConnectionsRequest {}
message GetDeviceProfilesRequest {}

message ArtnetConfig {
  string name = 1;
  string host = 2;
  uint32 port = 3;
}

message SacnConfig {
  string name = 1;
}

message Connections {
  repeated Connection connections = 1;
}

message Connection {
  string name = 1;
  oneof connection {
    DmxConnection dmx = 10;
    MidiConnection midi = 11;
    OscConnection osc = 12;
    ProDjLinkConnection proDJLink = 13;
    HeliosConnection helios = 14;
    EtherDreamConnection etherDream = 15;
    GamepadConnection gamepad = 16;
  }
}

message DmxConnection {
  string outputId = 1;
  oneof config {
    ArtnetConfig artnet = 3;
    SacnConfig sacn = 4;
  }
}

message HeliosConnection {
  string name = 1;
  uint32 firmware = 2;
}

message EtherDreamConnection {
  string name = 1;
}

message GamepadConnection {
  string id = 1;
  string name = 2;
}

message MidiConnection {
  optional string device_profile = 1;
}

message MidiDeviceProfiles {
  repeated MidiDeviceProfile profiles = 1;
}

message MidiDeviceProfile {
  string id = 1;
  string manufacturer = 2;
  string model = 3;
  optional string layout = 4;
  repeated Page pages = 5;

  message Page {
    string name = 1;
    repeated Group groups = 2;
    repeated Control controls = 3;
  }

  message Group {
    string name = 1;
    repeated Control controls = 2;
  }

  message Control {
    string id = 1;
    string name = 2;
    uint32 channel = 3;
    uint32 note = 4;
    ControlType control_type = 5;
    bool has_output = 6;
  }

  enum ControlType {
    Note = 0;
    CC = 1;
  }
}

message OscConnection {
  uint32 input_port = 1;
  uint32 output_port = 2;
  string output_address = 3;
}

message ProDjLinkConnection {
  string address = 1;
  string model = 2;
  uint32 playerNumber = 3;
  CdjPlayback playback = 5;
}

message CdjPlayback {
  bool live = 1;
  double bpm = 2;
  uint32 frame = 3;
  State playback = 4;
  Track track = 5;

  enum State {
    Loading = 0;
    Playing = 1;
    Cued = 2;
    Cueing = 3;
  }

  message Track {
    string artist = 1;
    string title = 2;
  }
}

message ConfigureConnectionRequest {
  oneof config {
    DmxConnection dmx = 1;
  }
}
